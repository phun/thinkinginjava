package com.fuhao.enumtest;

public enum Spiciness {
	NOT, MILD, MEDIUM, HOT, FLAMING("a");
	private String description;
	private Spiciness(String description) {
		this.description = description;
	}
	private Spiciness() {
		this.description = "";
	}
}

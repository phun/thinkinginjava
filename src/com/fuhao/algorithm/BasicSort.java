package com.fuhao.algorithm;

public class BasicSort {
	/**
	 * 在这里学习Java中参数的引用的方法。
	 * 使用“=”则是传值;
	 * 使用一个类的方法则是传引用。
	 * @param data
	 * @param a
	 * @param b
	 */
	public static void swap(int[] data, int a, int b) {
		int t = data[a];
		data[a] = data[b];
		data[b] = t;
	}

	public static void main(String[] args) {
		int[] data = new int[10];
		for (int i = 0; i < 10; i++) {
			data[i] = (int) (Math.random() * 100);
			System.out.print(" " + data[i]);
		}
		System.out.println();
		for (int i = 0; i < 9; i++) {
			for (int j = i; j < 10; j++) {
				if (data[i] > data[j]) {
					swap(data, i, j);
				}
			}
		}
		for (int i = 0; i < 10; i++) {
			System.out.print(" " + data[i]);
		}
		System.out.println();
	}
}

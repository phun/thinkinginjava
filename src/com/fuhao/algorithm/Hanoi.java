package com.fuhao.algorithm;

public class Hanoi {
	private static int sum = 0;
	
	/**
	 * 
	 * @param n 层数
	 * @param a 起始
	 * @param b 中转
	 * @param c 目标
	 */
	public static void hanoi(int n, String a, String b, String c) {
		if (n == 1) {
			move(1, a, c);
		} else {
			hanoi(n-1, a, c, b);	// 先把上面 n-1 层搬到 b
			move(n, a, c);			// 在把最下边那个搬到 c
			hanoi(n-1, b, a, c);	// 再把 b 上的搬到 C
		}
	}
	
	public static void move (int n, String a, String b) {
		System.out.println("The " + n + " floor: " + a + " => " + b);
		sum++;
	}
	
	public static void main(String[] args) {
		Hanoi.hanoi(3, "a", "b", "c");
		System.out.println("Move " + sum + " times!");
	}
}
